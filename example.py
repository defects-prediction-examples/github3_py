import os
import re
from typing import List

from dotenv import load_dotenv, find_dotenv

from github3 import *
from github3.issues.label import Label
from github3.null import NullObject
from github3.search import IssueSearchResult

load_dotenv(find_dotenv())

TOKEN = os.environ.get('TOKEN')

g = login(token=TOKEN)
me = g.user('sudobobo')

# let's iterate over my projects
for repo in g.repositories_by(me):
    pass

# let's find all bug-like issues and diff-urls for closing commits

# repo = g.repository(me, 'socket-chat')
# for issue_search_result in g.search_issues('repo:SudoBobo/socket-chat'):
#     issue = issue_search_result.issue
#     if 'bug' in map(lambda x: x.name, issue.labels()):
#         if not (isinstance(issue.pull_request(), NullObject)):
#             # print(issue.pll_request_urls['patch_url'])
#             print(i)
#             i += 1
#             print(issue.pull_request().diff())
#



issue = g.issue('SudoBobo', 'socket-chat', 3)
pull_request = issue.pull_request()
print(pull_request.diff())
def get_repo(owner: str, repository_name: str) -> Repository:
    """
    Load repository info for given owner and repository_name.
    Example: 'gradle/gradle' -> owner='gradle', repository_name='gradle'

    :param owner: name of GitHub user with this repo
    :param repository_name:
    :return: Github.Repository object
    """
    return Github(GITHUB_TOKEN).get_user(owner).get_repo(repository_name)


def is_bug_label(label: str) -> bool:
    """
    Matches bug regex

    :return: true if label match bug regex and false otherwise
    """
    regex = re.compile(r'^.*([Bb]ug).*$')
    return bool(regex.match(label))


def get_bug_like_labels(repo: Repository) -> List[Label]:
    """
    Returns all bug-like labels from given repository

    :param repo: Repository object
    :return: List[Label]
    """
    return list(filter(lambda label: is_bug_label(label.name), repo.get_labels()))


def get_issues_with_bug_labels(repo: Repository) -> PaginatedList:
    """
    Return all issues with tags like 'bug'.

    :param repo: Repository object
    :return: issues from repository with bug-like labels
    """
    bug_like_labels = get_bug_like_labels(repo)
    return repo.get_issues(state='closed', labels=bug_like_labels)


def get_diff_url_for_issue(issue: Issue) -> str:
    """
    Returns diff url for pull request associated with issue

    :param issue:
    :return:
    """
    diff_url = issue.pull_request.diff_url
    return diff_url


if __name__ == '__main__':
    angular_repo = get_repo('angular', 'angular.js')
    print('Repository: {}'.format(angular_repo))

    bug_likes_labels = get_bug_like_labels(angular_repo)
    print('Bug-like labels: {}'.format(bug_likes_labels))

    issues_with_bug_labels = get_issues_with_bug_labels(angular_repo)
    print('Closed issues for bugs: {}'.format(issues_with_bug_labels))

    diff = get_diff_url_for_issue(issues_with_bug_labels[0])
    print(diff)
